let chai = require('chai');
let expect = chai.expect;
let chaiHttp = require('chai-http');
let should = chai.should();

chai.use(chaiHttp);

// describe('/First Test Collection', function() {

//     it('should test two value.........', function() {
//         //actual test content in here
//         let expectedVal = 10;
//         let actualVal = 10;

//         expect(actualVal).to.be.equal(expectedVal);

//     })
// })

describe('GET /doctor/all-records', () => {

    var host = "http://localhost:2504";
    it('Test case is run successfully and now generate the build', (done) => {
    chai.request(host)
      .get('/doctor/all-records')
      .end((err, res) => {  
        res.should.have.status(200);
        res.body.data.should.be.a('array');
        res.body.data.length.should.be.eq(3);
        done();
      });
  });
});