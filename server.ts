import app from './app';
import * as http from 'http';
import { ENV } from './config/config.json'
import { MongoDB } from './config/mongo-config';
const { Pool, Client } = require("pg");

const port = 2504;

http.createServer(app).listen(port, () => {
  console.log('Express server listening on port ' + port);
  //const mdb = new MongoDB();
  //mdb.connectMongoDB();
});

const credentials = {
  user: ENV.PGUSER,
  host: ENV.PGHOST,
  database: ENV.PGDATABASE,
  password: ENV.PGPASSWORD,
  port: ENV.PGPORT,
};

var pool = new Pool(credentials);

module.exports = pool;
// Connect with a connection pool.

async function poolDemo() {
  const pool = new Pool(credentials);
  const now = await pool.query("SELECT NOW()");
  await pool.end();

  return now;
}

// Connect with a client.

async function clientDemo() {
  const client = new Client(credentials);
  await client.connect();
  const now = await client.query("SELECT NOW()");
  await client.end();

  return now;
}

// Use a self-calling function so we can use async / await.

(async () => {
  const poolResult = await poolDemo();
  console.log("Time with pool: " + poolResult.rows[0]["now"]);

  const clientResult = await clientDemo();
  console.log("Time with client: " + clientResult.rows[0]["now"]);
})();