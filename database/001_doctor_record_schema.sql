CREATE TABLE IF NOT EXISTS doctor_records(
 doctorid int NOT NULL AUTO_INCREMENT,
 `name` varchar(255) NOT NULL,
 `mobile` varchar(255) NOT NULL,
 `email` varchar(255) NOT NULL,
 `city` varchar(255) NOT NULL,
 `state` varchar(255) NOT NULL,
 `specialization` varchar(255) NOT NULL,
 `prescriber_type` varchar(255) NOT NULL,
 `custom_5` varchar(255) NOT NULL,
 `degree` varchar(255) NOT NULL,
 `department` varchar(255) NOT NULL,
 `custom1` varchar(255) NOT NULL,
 `custom2` varchar(255) NOT NULL,
 createdAt timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
 updatedAt timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
);