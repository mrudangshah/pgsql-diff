import { Router } from "express";
import { DoctorController } from "./doctor-records.controller";
//import * as fs from "fs-extra";
//import multer from 'multer';

let folderName = "doctor-records";
let datenow = Date.now;


class DoctorRoutes {
  private doctorController: DoctorController = new DoctorController;
  router: Router;
  constructor() {
    this.router = Router();
    this.init();
  }

  init() {
    this.router.get("/all-records", this.doctorController.getAll);
  }
}
const doctorRoutes = new DoctorRoutes();
doctorRoutes.init();
export default doctorRoutes.router;