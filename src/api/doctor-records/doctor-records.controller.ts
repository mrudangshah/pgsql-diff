import { Request, Response, NextFunction } from "express";
import { config, ENV } from '../../../config/config.json';
import { DoctorRecordRepository } from '../../repository/doctor-records/doctor-records.repository';
import { isEmpty } from "../../../config/config.json";
import { UserModel } from "../../models/user.model";
//import { MSGS } from "./user.constants";
//import { UserValidator } from "./user.validator";
import { ActionType, ReturnValue } from "../../../config/common";

export class DoctorController {

  public async getAll(req: Request, res: Response, next: NextFunction) {
    try {

      //let userModel: UserModel[], statusCode: number;
      let doctorrecordRepository = new DoctorRecordRepository;

      doctorrecordRepository = await doctorrecordRepository.getAll();
      const statusCode = isEmpty(doctorrecordRepository) ? config.statusCode.empty : config.statusCode.successful;
      res.status(statusCode).json({ data: doctorrecordRepository });
    } catch (err) {
      res.status(config.statusCode.internalServer).json({ error: err.message });
    }
  }

}