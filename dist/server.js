"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
exports.__esModule = true;
var app_1 = require("./app");
var http = require("http");
var config_json_1 = require("./config/config.json");
var _a = require("pg"), Pool = _a.Pool, Client = _a.Client;
var port = 2504;
http.createServer(app_1["default"]).listen(port, function () {
    console.log('Express server listening on port ' + port);
    //const mdb = new MongoDB();
    //mdb.connectMongoDB();
});
var credentials = {
    user: config_json_1.ENV.PGUSER,
    host: config_json_1.ENV.PGHOST,
    database: config_json_1.ENV.PGDATABASE,
    password: config_json_1.ENV.PGPASSWORD,
    port: config_json_1.ENV.PGPORT
};
var pool = new Pool(credentials);
module.exports = pool;
// Connect with a connection pool.
function poolDemo() {
    return __awaiter(this, void 0, void 0, function () {
        var pool, now;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    pool = new Pool(credentials);
                    return [4 /*yield*/, pool.query("SELECT NOW()")];
                case 1:
                    now = _a.sent();
                    return [4 /*yield*/, pool.end()];
                case 2:
                    _a.sent();
                    return [2 /*return*/, now];
            }
        });
    });
}
// Connect with a client.
function clientDemo() {
    return __awaiter(this, void 0, void 0, function () {
        var client, now;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    client = new Client(credentials);
                    return [4 /*yield*/, client.connect()];
                case 1:
                    _a.sent();
                    return [4 /*yield*/, client.query("SELECT NOW()")];
                case 2:
                    now = _a.sent();
                    return [4 /*yield*/, client.end()];
                case 3:
                    _a.sent();
                    return [2 /*return*/, now];
            }
        });
    });
}
// Use a self-calling function so we can use async / await.
(function () { return __awaiter(void 0, void 0, void 0, function () {
    var poolResult, clientResult;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0: return [4 /*yield*/, poolDemo()];
            case 1:
                poolResult = _a.sent();
                console.log("Time with pool: " + poolResult.rows[0]["now"]);
                return [4 /*yield*/, clientDemo()];
            case 2:
                clientResult = _a.sent();
                console.log("Time with client: " + clientResult.rows[0]["now"]);
                return [2 /*return*/];
        }
    });
}); })();
