"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.dbPool = exports.pool = void 0;
const mysql = require('mysql');
const util = require('util');
const config_json_1 = require("./config.json");
exports.pool = mysql.createPool({
    host: config_json_1.ENV.DB_HOST,
    user: config_json_1.ENV.DB_USER,
    password: config_json_1.ENV.DB_PASSWORD,
    database: config_json_1.ENV.DB_NAME
});
exports.dbPool = {
    query: (text) => util.promisify(exports.pool.query),
    pool: exports.pool
};
exports.pool.query = util.promisify(exports.pool.query);
exports.pool.getConnection = util.promisify(exports.pool.getConnection);
