"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const config_json_1 = require("./config.json");
const { Pool, Client } = require("pg");
const credentials = {
    user: config_json_1.ENV.PGUSER,
    host: config_json_1.ENV.PGHOST,
    database: config_json_1.ENV.PGDATABASE,
    password: config_json_1.ENV.PGPASSWORD,
    port: config_json_1.ENV.PGPORT,
};
// Connect with a connection pool.
async function poolDemo() {
    const pool = new Pool(credentials);
    const now = await pool.query("SELECT NOW()");
    await pool.end();
    return now;
}
// Connect with a client.
async function clientDemo() {
    const client = new Client(credentials);
    await client.connect();
    const now = await client.query("SELECT NOW()");
    await client.end();
    return now;
}
// Use a self-calling function so we can use async / await.
(async () => {
    const poolResult = await poolDemo();
    console.log("Time with pool: " + poolResult.rows[0]["now"]);
    const clientResult = await clientDemo();
    console.log("Time with client: " + clientResult.rows[0]["now"]);
})();
