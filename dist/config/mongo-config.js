"use strict";
exports.__esModule = true;
exports.MongoDB = void 0;
var mongoose_1 = require("mongoose");
var config_json_1 = require("./config.json");
var MongoDB = /** @class */ (function () {
    function MongoDB() {
    }
    MongoDB.prototype.connectMongoDB = function () {
        try {
            var isCluster = config_json_1.ENV.IS_MONGODB_CLUSTER;
            if (isCluster) {
                (0, mongoose_1.connect)("".concat(config_json_1.ENV.MONGODB_CLUSTER_URL), { useUnifiedTopology: true, useNewUrlParser: true });
            }
            else {
                if (config_json_1.ENV.MONGODB_USER && config_json_1.ENV.MONGODB_PASSWORD) {
                    (0, mongoose_1.connect)("mongodb://".concat(config_json_1.ENV.MONGODB_USER, ":").concat(config_json_1.ENV.MONGODB_PASSWORD, "@").concat(config_json_1.ENV.MONGODB_HOST, ":").concat(config_json_1.ENV.MONGODB_PORT, "/").concat(config_json_1.ENV.MONGODB_NAME), { useUnifiedTopology: true, useNewUrlParser: true });
                }
                else {
                    (0, mongoose_1.connect)("mongodb://".concat(config_json_1.ENV.MONGODB_HOST, ":").concat(config_json_1.ENV.MONGODB_PORT, "/").concat(config_json_1.ENV.MONGODB_NAME), { useUnifiedTopology: true, useNewUrlParser: true });
                }
            }
        }
        catch (err) {
            throw err;
        }
    };
    return MongoDB;
}());
exports.MongoDB = MongoDB;
