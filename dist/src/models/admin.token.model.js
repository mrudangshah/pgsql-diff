"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AdminToken = void 0;
const mongoose_1 = require("mongoose");
const admin_token_schema_1 = require("../repository/mongoDB/schema/admin-token.schema");
const AdminToken = mongoose_1.model('AdminToken', admin_token_schema_1.AdminTokenSchema);
exports.AdminToken = AdminToken;
