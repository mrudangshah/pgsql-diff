"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.LoginController = void 0;
const jwt = __importStar(require("jsonwebtoken"));
const admin_auth_repository_1 = require("../../repository/mongoDB/admin-auth/admin-auth.repository");
class LoginController {
    async checkResponse(req, res, next) {
        try {
            res.status(200).json({ message: "Hello World" });
        }
        catch (error) {
            next(error);
        }
    }
    async login(req, res, next) {
        try {
            const adminlogin = {
                user_name: "John Doe",
                user_email: "test@malinator.com",
                password: "test@1234"
            };
            let repositry = new admin_auth_repository_1.AdminAuthRepository();
            const token = await jwt.sign({ adminlogin }, "myRandomJWTToken");
            let login = await repositry.createAdminAuth(adminlogin, token);
            res.status(200).json({ data: login, message: "LOGIN_SUCCESSFUL" });
        }
        catch (error) {
            next(error);
        }
    }
    async redirectLogin(req, res, next) {
        try {
            const adminlogin = {
                user_name: "John Doe",
                user_email: "test@malinator.com",
                password: "test@1234"
            };
            let repositry = new admin_auth_repository_1.AdminAuthRepository();
            const token = await jwt.sign({ adminlogin }, "myRandomJWTToken");
            let login = await repositry.createAdminAuth(adminlogin, req.body.token);
            res.status(200).json({ data: login, message: "LOGIN_SUCCESSFUL" });
        }
        catch (error) {
            next(error);
        }
    }
}
exports.LoginController = LoginController;
