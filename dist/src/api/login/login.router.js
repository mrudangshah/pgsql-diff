"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const login_controller_1 = require("./login.controller");
class LoginRoutes {
    constructor() {
        this.loginController = new login_controller_1.LoginController();
        this.router = express_1.Router();
        this.init();
    }
    init() {
        this.router.get("/", this.loginController.checkResponse);
        this.router.post("/", this.loginController.login);
        this.router.post("/redirect", this.loginController.redirectLogin);
    }
}
const loginRoutes = new LoginRoutes();
loginRoutes.init();
exports.default = loginRoutes.router;
