"use strict";
exports.__esModule = true;
var express_1 = require("express");
var doctor_records_controller_1 = require("./doctor-records.controller");
//import * as fs from "fs-extra";
//import multer from 'multer';
var folderName = "doctor-records";
var datenow = Date.now;
var DoctorRoutes = /** @class */ (function () {
    function DoctorRoutes() {
        this.doctorController = new doctor_records_controller_1.DoctorController;
        this.router = (0, express_1.Router)();
        this.init();
    }
    DoctorRoutes.prototype.init = function () {
        this.router.get("/all-records", this.doctorController.getAll);
    };
    return DoctorRoutes;
}());
var doctorRoutes = new DoctorRoutes();
doctorRoutes.init();
exports["default"] = doctorRoutes.router;
