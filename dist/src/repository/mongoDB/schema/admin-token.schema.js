"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AdminTokenSchema = void 0;
const mongoose_1 = require("mongoose");
exports.AdminTokenSchema = new mongoose_1.Schema({
    userName: String,
    userEmail: String,
    token: String
});
