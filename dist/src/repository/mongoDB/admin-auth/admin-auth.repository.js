"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AdminAuthRepository = void 0;
const admin_token_model_1 = require("../../../models/admin.token.model");
class AdminAuthRepository {
    async createAdminAuth(userDetails, token) {
        try {
            let adminTokenDetails = new admin_token_model_1.AdminToken({
                userName: userDetails.user_name,
                userEmail: userDetails.user_email,
                token: token
            });
            let insertToken = await adminTokenDetails.save();
            let result = insertToken.id ? true : false;
            if (result === true) {
                return insertToken;
            }
            else if (result === false) {
                return false;
            }
        }
        catch (err) {
            throw err;
        }
    }
}
exports.AdminAuthRepository = AdminAuthRepository;
